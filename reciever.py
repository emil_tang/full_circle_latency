from time import perf_counter_ns
from serial import Serial

DEVICE_0: str = '/dev/ttyACM0'
DEVICE_1: str = '/dev/ttyACM1'
B_RATE: int = 115200


def receive():
    """Recive bytes from serial port"""
    with open("after", "a+") as after, Serial(DEVICE_1, B_RATE, timeout=0) as port:
        while True:
            if port.in_waiting > 0:
                byte = port.read(1)
                out = ord(byte)
                after.write(f'{out} {perf_counter_ns()}\n')
                print(out)


if __name__ == "__main__":
    receive()
