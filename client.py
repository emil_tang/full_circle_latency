"""Full Cycle Latency Test"""
import asyncio
from time import time_ns
from typing import Iterator
from serial import Serial

DEVICE_0: str = '/dev/ttyACM0'
DEVICE_1: str = '/dev/ttyACM1'
B_RATE: int = 115200


def byte_generator() -> Iterator[int]:
    """Generate and infinite series of ones and zeros"""
    state = 1
    while True:
        if state == 1:
            yield state
            state = 0
        else:
            yield state
            state = 1


async def send_signal():
    """Send bytes to serial port"""
    with open("before", "a+") as before, Serial(DEVICE_0, B_RATE) as port:
        for signal in byte_generator():
            port.write(bytes([signal]))
            before.write(f'{signal} {time_ns()}\n')
            print(signal)
            await asyncio.sleep(1)


async def receive():
    """Recive bytes from serial port"""
    with open("after", "a+") as after, Serial(DEVICE_1, B_RATE, timeout=0) as port:
        while True:
            if port.in_waiting > 0:
                byte = port.read(1)
                out = ord(byte)
                after.write(f'{out} {time_ns()}\n')
                print(out)
            await asyncio.sleep(0.001)


async def main():
    """Main"""
    await asyncio.gather(send_signal(), receive())

if __name__ == "__main__":
    asyncio.run(main())
