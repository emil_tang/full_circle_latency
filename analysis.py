from typing import Sequence, TextIO, Tuple, Iterator
from matplotlib.pyplot import hist, show, plot, scatter, savefig, xlabel, ylabel, boxplot
from statistics import mean, median, stdev

def test_missing_entries(lst: Sequence) -> bool:
    prev = lst[0]
    for i in range(1, len(lst)):
        curr = lst[i]
        if prev == curr:
            return False
        prev = curr
    return True


def test_same_length(lst0: Sequence, lst1: Sequence) -> bool:
    return len(lst0) == len(lst1)


def unmarshal(in_file: TextIO) -> Sequence[Tuple[str, int]]:
    lines = [line.strip().split(' ') for line in in_file.readlines()]
    return [(line[0], int(line[1])) for line in lines]


def get_times(lst: Sequence) -> Iterator:
    return map(lambda a: a[1], lst)


def difference(lst0: Sequence[Tuple[str, int]], lst1: Sequence[Tuple[str, int]]) -> Sequence[float]:
    return [x[1] - x[0] for x in zip(get_times(lst0), get_times(lst1))]


def main():
    with open("before", "r") as before, open("after", "r") as after:
        before_t = unmarshal(before)
        after_t = unmarshal(after)

        assert test_missing_entries(before_t)
        assert test_missing_entries(after_t)
        assert test_same_length(before_t, after_t)

       
        diff = difference(before_t, after_t)
        print(max(diff), min(diff), mean(diff), median(diff),stdev(diff))
        
        # hist(diff,bins=range(min(diff), max(diff) + 100000000, 100000000)) 
        #boxplot(diff)
        # xlabel('Time Difference')
        # scatter(list(get_times(before_t))[1000:1100],diff[1000:1100],marker=".")
        # plot(diff, linestyle="None", marker=".")
        # xlabel('Elapsed Time')
        # ylabel('Time Difference')
        # show()
        savefig('box.pdf')


if __name__ == "__main__":
    main()
