import pycom
import os
from machine import UART
from LTR329ALS01 import LTR329ALS01

LIGHT_THRESHOLD = 10
OFF = 0x000000

if __name__ == "__main__":
    pycom.heartbeat(False)
    pycom.rgbled(OFF)
    sensor = LTR329ALS01(rate=LTR329ALS01.ALS_RATE_50)
    uart = UART(0, baudrate=115200)
    uart.init(115200, bits=8, parity=None, stop=1)
    os.dupterm(uart)

    old_state =  False

    while True:
        light_value = sensor.light()
        new_state = (light_value[0] >= LIGHT_THRESHOLD)
        if new_state != old_state:
            old_state = new_state
            uart.write(b'\x01' if old_state else b'\x00')

