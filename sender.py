from typing import Iterator

from serial import Serial
from time import perf_counter_ns, sleep

DEVICE_0: str = '/dev/ttyACM0'
DEVICE_1: str = '/dev/ttyACM1'
B_RATE: int = 115200


def byte_generator() -> Iterator[int]:
    """Generate and infinite series of ones and zeros"""
    state = 1
    while True:
        if state == 1:
            yield state
            state = 0
        else:
            yield state
            state = 1


def send_signal():
    """Send bytes to serial port"""
    with open("before", "a+") as before, Serial(DEVICE_0, B_RATE) as port:
        for signal in byte_generator():
            before.write(f'{signal} {perf_counter_ns()}\n')
            print(signal)
            port.write(bytes([signal]))
            sleep(2)


if __name__ == "__main__":
    send_signal()
