import os
import pycom
from machine import UART

pycom.heartbeat(False)

uart = UART(0, 115200)
uart.init(115200, bits=8, parity=None, stop=1)
os.dupterm(uart)

while True:
    n = uart.any()
    if n != 0:
        b = uart.read(n)
        if b == b'\x01':
            pycom.rgbled(0xFFFFFF)
        elif b == b'\x00':
            pycom.rgbled(0)
